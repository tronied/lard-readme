# Language Architect and Runtime Development (LARD) 
LARD is a tool that makes it easy to create interpreted programming languages. It does this by providing a high-level
API that allows you to focus on the building blocks of your language, called tokens. Tokens are matched from expressions
and represent operators, literals and statements. You can also customize other aspects of your language, such as its
core functions, expression notations, and precedence rules. This means that you can create a language that is tailored
to your specific needs without having to worry about the low-level details of lexing and parsing.

The default library gives you a clean slate to create your language from scratch. There are however several starter
projects and a tutorial to guide you through everything from defining literals and operations all the way up to custom
types. LARD can handle both typed and typeless languages, so let your imagination run wild and create that language
you've always dreamed of.

## Why should you use it?
Writing programming languages is difficult because it requires a deep understanding of computer science concepts,
such as logic, data structures, and algorithms. It also requires a strong understanding of the syntax and semantics of
the programming language being created. In addition, writing a programming language takes a long time because it is a
complex process that involves many different steps. These steps include:

- Defining the language's grammar and syntax
- Designing the language's data types and operators
- Implementing the language's runtime system
- Writing the language's standard library
- Testing the language's correctness and performance
- Documenting the language's features and usage

Even after all of these steps have been completed, the language may still need to be improved or updated over time.
This is because new features are constantly being developed, and the language needs to be able to adapt to changes in
as new ideas are introduced. This is where LARD can help as it is built around the concept of adaptability and
change. LARD also provides the following:

- Simple to learn and use: The LARD API is well-documented and easy to understand. Even if you have no prior experience
  with language development, you should be able to get up and running with LARD quickly.
- Fast development time: LARD takes care of the low-level details of lexing and parsing so that you can focus on the
  more important aspects of language development, such as syntax and semantics. This can save you a lot of time and effort.
- Tried and tested lexer: LARD has an extensive testing suite to ensure correct behaviour. This means that you can be
  confident that your language will be able to correctly parse a wide range of input.
- Powered by Java: LARD is written in Java, which is a multi-platform, secure, and a well-supported programming language.
  This means that your language will be able to run on a wide range of platforms, including Windows, macOS, and Linux and
  make use of a large number of underlying frameworks to enhance your language.
- Open source: LARD is open source meaning that it is free to use and modify. This can be a valuable benefit should you
  wish to customize LARD to meet your specific needs.
- Actively maintained: LARD has been two years in the making, is actively maintained and has a lot of planned features
  in the pipeline. You can be confident that LARD will continue to be supported and updated. If you do spot any issues,
  please do send an email or raise them on gitlab and these will be fixed ASAP.

## Tutorial
The following guide takes you through the steps to create a language from scratch. It's broken into bite-size sections
covering everything from setting up your project all the way up to defining example types and objects.
### Getting Started
Start a new project in your favourite IDE, give it a name (I've called mine Aardvark) and add the following dependency
to gradle:
```groovy
dependencies {
    implementation 'dev.lard:lard-core:1.0'
}
```
or Maven:
```xml
<dependencies>
    <dependency>
        <groupId>dev.lard</groupId>
        <artifactId>lard-core</artifactId>
        <version>1.0</version>
    </dependency>
</dependencies>
```
A projects structure can differ depending on your chosen build tool / IDE, but it should have a structure similar
to the following:
```
[gradle]
[src]
   [main]
       [java]
           [com.aardvark]
               Application.java
   [test]
        ... 
build.gradle
gradlew
gradlew.bat
settings.gradle
```
The first thing to do will be to create a configuration file. This is where all your tokens, operators and properties
are defined which determine how your language will look and operate. We'll first add a config folder to the structure
and put our new class in that:
```
...
[src]
   [main]
       [java]
           [com.aardvark]
               [config]
                   AardvarkConfig.java
               Application.java
...
```
Extending our class from LARDConfig will require us to implement several methods:
```java
public class AardvarkConfig extends LARDConfig {

  @Override
  protected void initTokenHandlers() { }

  @Override
  protected void initFunctions() { }

  @Override
  protected TypeOperation initTypeOperations() { retuurn null; }

  @Override
  protected void initOperators() { }

  @Override
  protected void initParserFormatters() { }
}
```
Don't worry about the content of these methods as we'll get to these in due time. Next we'll modify the existing
Application.java file in the root of the project directory. We'll change it to extend another class called LARDRunner*
and define the following in the main method:
```java
public class Application extends LARDRunner {

  public static void main(String[] args) {
    AardvarkConfig config = new AardvarkConfig();
    LARDProcessor processor = new LARDProcessor(config);
    run("Aardvark Language", processor);
  }
}
```
*The LARDRunner is a utility class which allows you to test out your language as you develop it.

Run the application and you'll be presented with the following:
```
Aardvark Language Test Utility
==============================

```
From here you'll be able to evaluate expressions and debug your code. Now that we have the basics sorted, let's get
started writing our language.

### Literals
#### Null Token
In the beginning there was null... well actually, not even that as we haven't defined it yet! You can test this by
typing null in our runner app:
```
Aardvark Language Test Utility
===============================
null
dev.lard.exception.ParserException: Unexpected token 'null' found in expression. Strict syntax checking is enabled
	at dev.lard.lexer.LARDLexer.analysePatterns(LARDLexer.java:98)
	at dev.lard.lexer.LARDLexer.tokenize(LARDLexer.java:57)
	at dev.lard.lexer.Lexer.tokenize(Lexer.java:49)
	at dev.lard.processor.LARDProcessor.process(LARDProcessor.java:82)
	at dev.lard.runner.LARDRunner.run(LARDRunner.java:39)
	at dev.lard.runner.LARDRunner.run(LARDRunner.java:10)
	at dev.lard.runner.Application.main(Application.java:10)
```
As such, this will be our first task. Create a new package called tokens. Within that add another package called
literals and add a new file called NullToken.java:
```
...
[src]
   [main]
       [java]
           [com.aardvark]
               [config]
                   AardvarkConfig.java
               [tokens]
                   [literals]
                       NullToken.java
               Application.java
...
```
Open up the new token class and add ``extends Token<Void>`` after the class name. As with the configuration file,
this will inherit a number of methods to implement from the parent class. These can be seen here:
```java
public class NullToken extends Token<Void> {

  @Override
  public Token<Void> createToken(String value) {
    return null;
  }

  @Override
  public PatternType getPatternType() {
    return null;
  }

  @Override
  public String getPattern() {
    return null;
  }

  @Override
  public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
    return Optional.empty();
  }

  @Override
  protected List<Token<?>> process(LARDParser parser, LARDContext context, LARDConfig config) {
    return null;
  }
}
```
I'll briefly run through the features of this class before diving in. Firstly, you'll notice that the Token class
we are extending always requires a generic type. This determines the type each token will represent / store as a value.
In our case, since there is no Null type in Java, we can use Void. Here is a description of the other functions in
the class:
- **createToken**: Gets called when the lexer matches this tokens pattern against the beginning of the expression. It
  returns a new instance with the function parameter being the matched value. In this case it's null so we can just invoke
  the blank constructor as we won't store it.
- **getPatternType**: This is an enum representing the pattern type for the current token. This has two possible values
  being either REGEX or GRAMMAR. For simple literals (Integer, Float, Double, String etc) these can be matched using
  regular expressions. For more complex structures like statements or collections, those use grammar.
- **getPattern**: Dependent on the pattern type, this will either contain a regular expression or a grammar definition.
  For example, to define our null keyword we'll be using "^null" or "^nil" (whatever you prefer). As the expression is
  processed, its matched against what's left of the unprocessed expression String. The '^' before the keyword simply
  means that this is matched against the beginning of the String.
- **getGuidance**: This is used when we start defining statements. If the expression is not valid and the current active
  statement is still waiting for a given token, we can provide them a message here to show them where they made the mistake
  and how to fix it.
- **process**: During expression resolution, this gets called by the parser and this is where the bulk of our language
  code will go. For literals though like null, you will simply return the current token as-is.

Given the above, we can now populate our token with the following:
```java
public class NullToken extends Token<Void> {

  public NullToken() {
    super("null", null);
  }

  @Override
  public PatternType getPatternType() {
    return PatternType.REGEX;
  }

  @Override
  public String getPattern() {
    return "^null";
  }

  @Override
  public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
    return Optional.empty();
  }

  @Override
  public List<Token<?>> process(LARDParser parser, LARDContext context, LARDConfig config) {
    return Collections.singletonList(this);
  }

  @Override
  public Token<Void> createToken(String value) {
    return new NullToken();
  }
}
```
We have also defined a constructor which calls the parent. This call sets a unique name for the token and specifies a
value for the token, in this case it is just null. Congratulations, we have our first token! Now all we need to do is
add it to our configuration:
```java
public class AardvarkConfig extends LARDConfig {
  //...
  @Override
  protected void initTokenHandlers() {
    addTokenHandler(new NullToken());
  }
  //...
}
```
Go back to our runner, and...
```
Aardvark Language Test Utility
===============================
null
Result: null (Type: Null, Time taken: 19ms)
```
we get null being returned when it is typed in. Nothing too exciting, but something that is necessary to have in most
languages. If you were wondering, don't worry too much about the performance for this first call. You'll notice
repeated or different expressions revert to 1 millisecond or less. This is caused by the overhead of Java loading
the classes and resources for the first time.

#### Integer Token
So, we've got a language that only contains null. Let's move on to adding numbers so that we can start to think
about resolving some basic calculations. For this we'll need to add a new token for Integers. Create a new token
called IntegerToken in your project ``tokens.literals`` folder and define the following:
```java
public class IntegerToken extends Token<Integer> {

    public IntegerToken() { super("Integer", 0); }

    public IntegerToken(Integer value) { super("Integer", value); }

    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    @Override
    public String getPattern() {
        return "^-?[0-9]+";
    }

    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    @Override
    public List<Token<?>> process(LARDParser parser, LARDContext context, LARDConfig config) {
        return Collections.singletonList(this);
    }

    @Override
    public Token<Integer> createToken(String value) {
        return new IntegerToken(Integer.parseInt(value));
    }
}
```
This follows a very similar structure to the NullToken, but with a different regular expression pattern declared. In
this case we are declaring the starts with '^' character, followed by '-' (minus character), followed by a '?' which
makes the preceding character optional. Next we define a list of possible characters in a group to match. In this case
we want to match numeric digits in a String between 0 up to 9. The final '+' means we can have one or more of those.
Using this pattern we can define the full range of positive and negative integer values.

We've also added an overloaded version of the constructor which accepts an Integer value. This is because the Integer
token deviates from the null as its value is mutable and can be set. It is actually used from the ``createToken``
method by converting the value from the expression String. So long as we've got the defined regular expression right,
we should have no conversion issues.

Again, let's add this to our configuration class:
```java
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected void initTokenHandlers() {
        addTokenHandler(new NullToken());
        addTokenHandler(new IntegerToken());
    }
    //...
}
```
Launch our runner app and...
```
Aardvark Language Test Utility
==============================
-10
Result: -10 (Type: Integer, Time taken: 19ms)
10
Result: 10 (Type: Integer, Time taken: 1ms)
```
We now have simple Integer support in our language. Let's try to do a basic calculation:
```
10 + 10
dev.lard.exception.ParserException: Unexpected token '+' found in expression. Strict syntax checking is enabled
	at dev.lard.lexer.LARDLexer.analysePatterns(LARDLexer.java:98)
	at dev.lard.lexer.LARDLexer.tokenize(LARDLexer.java:57)
	at dev.lard.lexer.Lexer.tokenize(Lexer.java:49)
	at dev.lard.processor.LARDProcessor.process(LARDProcessor.java:82)
	at dev.lard.runner.LARDRunner.run(LARDRunner.java:39)
	at dev.lard.runner.LARDRunner.run(LARDRunner.java:10)
	at dev.lard.runner.Application.main(Application.java:10)
```
As with everything we need to tell LARD how to handle operators. This would be the same if we're teaching a
child how to count. First you start with basic numbers and increments. Once they've grasped that they can then move
onto addition, subtraction, multiplication and division etc. In our case and for this to work, we first need to define
a set of arithmetic operators.

### Operators
In programming there are multiple sets of operators, each with a different purpose. Within LARD there are several
pre-defined template classes you can use for most cases. These are:

    ArithmeticOperator
    AssignmentOperator
    BitwiseOperator
    ComparisonOperator
    LogicOperator

That given, you are not limited to just these should you want to define your own sets of operators. Should you wish to
do this you can create a new class extending OperatorHandler and pass it the operator types defined in an enum which
you pass as a generic e.g. ``class MyCustomOperators extends OperatorHandler<MyCustomOperatorsType> { .. }``. This can
either be an abstract template or concrete implementation. You can also override the order in which sets of operators
are called. You can do this by overriding the ``OperatorHandler.getHandlerOrder`` method. The lower a value the higher
priority those are given during execution. In any case, the default sets should cover the requirements of the majority
of languages.

#### Arithmetic Operator Handler
Let's start out with defining an arithmetic operator set so we can do some basic calculations. To do this, let's create
a new package within the tokens package called operators:

```
...
[src]
   [main]
       [java]
           [com.aardvark]
               [config]
                   AardvarkConfig.java
               [tokens]
                   [operators]
                       AardvarkArithmeticOperator.java
                   [literals]                       
                       IntegerToken.java
                       NullToken.java
               Application.java
...
```
Next we'll extend from the ArithmeticOperator class and inherit the methods:
```java
public class AardvarkArithmeticOperator extends ArithmeticOperator {

    public AardvarkArithmeticOperator(String value) {
        super(value);
    }

    @Override
    protected void initOperators() {
        addOperator("+", ArithmeticOperatorType.ADD);
        addOperator("-", ArithmeticOperatorType.SUBTRACT);
        addOperator("/", ArithmeticOperatorType.DIVIDE);
        addOperator("*", ArithmeticOperatorType.MULTIPLY);
        addOperator("%", ArithmeticOperatorType.MODULO);
    }

    @Override
    public String getPattern() {
        return "^(\\+|-|\\/|\\*|%)";
    }

    @Override
    public Token<String> createToken(String value) {
        return new AardvarkArithmeticOperator(value);
    }

    @Override
    public String[][] getOperatorOrder() {
        return new String[][] {
                {"/", "*", "%"},
                {"+", "-"}
        };
    }
}
```
There are three key methods in this class. The first is a new one called initOperators. This allows us to associate a
String value to one of the Arithmetic operator types. To keep things familiar, I'm using the standard C / Java notation,
but feel free to define your own preferred operator values.

Following on from this we have the typical Token.getPattern method. This contains the pattern of the operators defined
in the initOperators method*. The final method determines the order in which operators are executed for a given operator
type. In our case we will follow BODMAS which determines that multiplication / division come before addition /
subtraction. A 2-dimensional array is defined with the operators of higher priority placed higher up.

As with all other tokens, we need to add it into our config class:
```java
@Override
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected void initOperators() {
        addOperatorHandler(new AardvarkArithmeticOperator(null));
    }
    //...
}
```

*NOTE: Please ensure the order of your operators does not conflict. For example, if you define ADD and INCREMENT
operators, but the ADD precedes the latter, the increment will never be matched. This is because it will always
go with the first match found and set that as the operator value. As such, to prevent this we put the '++' in front
of the '+' to ensure we match the correct type e.g.``^(...|\\+\\+|\\+|...)`` rather than ``^(...|\\+|\\+\\+|...)``.

Let's load up our runner and now see what happens:
```
10 + 10
dev.lard.exception.ParserException: No handler for type operation with arguments of type IntegerToken and IntegerToken found.
	at dev.lard.parser.LARDParser.processOp(LARDParser.java:317)
	at dev.lard.parser.LARDParser.processExpression(LARDParser.java:113)
	at dev.lard.parser.LARDParser.process(LARDParser.java:44)
	at dev.lard.processor.LARDProcessor.process(LARDProcessor.java:83)
	at dev.lard.runner.LARDRunner.run(LARDRunner.java:39)
	at dev.lard.runner.LARDRunner.run(LARDRunner.java:10)
	at dev.lard.languages.aardvark.AardvarkRunner.main(AardvarkRunner.java:11)
```
Although we've defined our operators, LARD still doesn't know what it is supposed to do with it during parsing. This
is where we define type operations. These not only tell it what to do in the situation of an operator occurring,
but is also specific to given types. For example, you can add two Integers together using ``1 + 1`` in Java, but you
can't do ``[1,2] + [3,4]``. By defining a type operation for an array type and configuring it to handle
``ArithmeticOperatorType.ADD``, we can provide this easily. More on that later, but for now we'll start by defining
a new type operation.

#### Type Operation Handler
Let's create a new package called operations and create a new token called IntegerOperation.java:
```
...
[src]
   [main]
       [java]
           [com.aardvark]
               [config]
                   AardvarkConfig.java
               [operations]
                   IntegerOperation.java
               [tokens]
                   [operators]
                       AardvarkArithmeticOperator.java
                   [literals]
                       IntegerToken.java
                       NullToken.java
               Application.java
...
```
Our class will extend TypeOperation where we're required to implement 3 methods. These are ``canHandle``,
``handleCustomOperator`` and ``process``. The canHandle determines whether the current operation class can handle
whatever is being executed by the parser. We will return true if both the first and second tokens represent Integers
by using ``aToken.is(Integer.class)``. The second method (handleCustomOperator) acts as a fallback and only gets called
if no match was found. This is triggered if you don't define an action for all the operators within our ArithmeticOperator
class.

The process method is where we write the code to handle each operator for the supported types. We first extract each
Token value using the ``getValue(Class<?> aClass)`` method. We then map the operator to a specific operator set
(ArithmeticOperator in our case) and return the result of each operation. The ``wrap`` method simply wraps the result
in our Token class we defined earlier.
```java
public class IntegerOperation implements TypeOperation {

    @Override
    public boolean canHandle(Token<?> first, OperatorHandler<?> operator, Token<?> second) {
        return first.is(Integer.class) && second.is(Integer.class);
    }

    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorHandler<?> operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle integer operation with operator '%s'",
                operator.getValue()));
    }

    @Override
    public Token<?> process(LARDConfig config, LARDContext context, Token<?> firstToken, OperatorHandler<?> operator,
                            Token<?> secondToken, Token<?> leftSide) {
        //Extract token values
        Integer first = firstToken.getValue(Integer.class);
        Integer second = secondToken.getValue(Integer.class);
        //Check that operator is Arithmetic and handle each case using switch
        if (operator instanceof ArithmeticOperator) {
            switch (((ArithmeticOperator)operator).getOpType()) {
                case ADD: return wrap(first + second);
                case SUBTRACT: return wrap(first - second);                
                case DIVIDE: return wrap(first / second);
                case MULTIPLY: return wrap(first * second);
                case MODULO: return wrap(first % second);
            }
        }
        //Fallback
        return handleCustomOperator(firstToken, operator, secondToken);
    }

    private Token<?> wrap(Integer result) {
        return new IntegerToken(result);
    }
}
```
Next we add this to our config class:
```java
@Override
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected TypeOperation initTypeOperations() {
        addTypeOperation(new IntegerOperation());
        return null;
    }
    //...
}
```
We'll return null from the initTypeOperations method for now. This simply states which type operation will be used as a
fallback should no other match be found. Now we have this, let's test out some basic calculations in our runner:
```
10 + 10
Result: 20 (Type: Integer, Time taken: 11ms)
10 / 2
Result: 5 (Type: Integer, Time taken: 1ms)
10 % 3
Result: 1 (Type: Integer, Time taken: 1ms)
1 + 6 / 3 + 1
Result: 4 (Type: Integer, Time taken: 2ms)
```
It's working! However not everything's perfect with our language. Take the following example:
```
3 / 2
Result: 1 (Type: Integer, Time taken: 0ms)
```
That's not quite right. Shouldn't a decimal value of 1.5 be returned? That's correct, but since we haven't defined
what a decimal is, how can one be returned? As such, let's fix this now. Let's add a new type for a Decimal (Double) by
creating a new DecimalToken.java in the ``tokens.literals`` package. This will be defined as the following:
```java
public class DecimalToken extends Token<Double> {

    public DecimalToken() { super("Double", 0.0); }

    public DecimalToken(Double value) {
        super("Double", value);
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    @Override
    public String getPattern() {
        return "^-?[0-9]+\\.[0-9]+";
    }

    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    @Override
    public List<Token<?>> process(LARDParser parser, LARDContext context, LARDConfig config) {
        return Collections.singletonList(this);
    }

    @Override
    public Token<Double> createToken(String value) {
        return new DecimalToken(Double.parseDouble(value));
    }
}
```
The pattern is really the only thing is note here. Again we're defining an optional '-' character preceding the number,
followed by one or more numbers from 0 to 9 separated by a '.'. Saying that, we could actually choose to make our
language adhere to the rules of the country it is running in. For example, we could use the following:
```java
public class DecimalToken extends Token<Double> {
    //...
    @Override
    public String getPattern() {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(Locale.getDefault());
        String n = df.format(1.1);
        return String.format("^-?[0-9]+\\%s[0-9]+", n.charAt(1));
    }
    //...
}
```
For Aardvark though I'll omit any localisation to keep things simple. Let's add our new token to the config:
```java
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected void initTokenHandlers() {
        addTokenHandler(new NullToken());
        addTokenHandler(new IntegerToken());
        addTokenHandler(new DecimalToken());
    }
    //...
}
```
Now let's go back to our IntegerOperation class. The natural behaviour of Java as well as other languages is to stick
to the types being used in the operation. As such, when we do ``Integer <divide> Integer``, we get back an Integer even
if we wanted to keep precision. There are two possibilities we can use. These are 1) Casting 2) Different types. We
will opt for the second option to keep things neat. Let's look back at the ``IntegerOperation.process`` method:

```java
public class IntegerOperation implements TypeOperation {
    //...
    @Override
    public Token<?> process(LARDConfig config, LARDContext context, Token<?> firstToken, OperatorHandler<?> operator,
                            Token<?> secondToken, Token<?> leftSide) {
      //Extract token values to BigDecimal
      BigDecimal first = new BigDecimal(firstToken.getValue(Integer.class));
      BigDecimal second = new BigDecimal(secondToken.getValue(Integer.class));
      //Check that operator is Arithmetic and handle each case using switch
      if (operator instanceof ArithmeticOperator) {
        switch (((ArithmeticOperator)operator).getOpType()) {
          case ADD: return wrap(first.add(second));
          case SUBTRACT: return wrap(first.subtract(second));
          case DIVIDE: return wrap(first.divide(second, 6, RoundingMode.HALF_UP));
          case MULTIPLY: return wrap(first.multiply(second));
          case MODULO: return wrap(first.remainder(second));
        }
      }
      //Fallback
      return handleCustomOperator(firstToken, operator, secondToken);
    }

    private Token<?> wrap(BigDecimal result) {
        return result.stripTrailingZeros().scale() >= 0 ?
            new DecimalToken(result.doubleValue()) :
            new IntegerToken(result.intValue());
    }
}
```
The following changes have made the code a lot more capable at the cost of some complexity. Firstly both Integer values
are cast to BigDecimal. This presumes that all operations have the possibility to spit out a remainder value. We've also
changed the behaviour of our wrap method to check if the result has a remainder a decimal value and return the
appropriate type. Let's try our runner again:
```
-10 / 3
Result: -3.333333 (Type: Double, Time taken: 11ms)
3 * 10
Result: 30 (Type: Integer, Time taken: 1ms)
6 / 4
Result: 1.5 (Type: Double, Time taken: 1ms)
5 + 3 / 2
dev.lard.exception.ParserException: No handler for type operation with arguments of type IntegerToken and DecimalToken found.
	at dev.lard.parser.LARDParser.processOp(LARDParser.java:317)
	at dev.lard.parser.LARDParser.replaceCalcsInfix(LARDParser.java:382)
	at dev.lard.parser.LARDParser.lambda$processExpression$0(LARDParser.java:74)
	at java.base/java.util.Spliterators$ArraySpliterator.forEachRemaining(Spliterators.java:948)
	at java.base/java.util.stream.ReferencePipeline$Head.forEach(ReferencePipeline.java:658)
	at dev.lard.parser.LARDParser.lambda$processExpression$1(LARDParser.java:74)
	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
	at java.base/java.util.stream.SortedOps$SizedRefSortingSink.end(SortedOps.java:357)
	...
```
This is where things turn a bit interesting. Firstly, we can now see that it is casting to either a Double or Integer
depending on the calculation. However, we get an error when try something a bit more complex. This is because the
result of ``3 / 2`` is 1.5. The next parser operation (given our use of BODMAS) will be 5 + 1.5 which we don't yet
support. Don't worry, I'm not going to recommend creating an operation class for every possible pairing as we can
make life easier for ourselves. Firstly, I'm going to rename IntegerOperation to NumericOperation:
```java
public class NumericOperation implements TypeOperation {

    @Override
    public boolean canHandle(Token<?> first, OperatorHandler<?> operator, Token<?> second) {
        try {
            if (Objects.isNull(first.getValue()) || Objects.isNull(second.getValue())) return false;
            new BigDecimal(first.getValue().toString());
            new BigDecimal(second.getValue().toString());
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }    
    //...
}
```
Secondly I've modified the operations canHandle method. The BigDecimal class is quite adept at handling any number of
different types, so it useful as a handler in this scenario. If whatever the token value is can be parsed then it means
we can handle it, otherwise we'll return false and defer to other operation classes.

Let's load up our runner once more:
```
-10 / 3
Result: -3.333333 (Type: Double, Time taken: 11ms)
3 * 10
Result: 30 (Type: Integer, Time taken: 1ms)
6 / 4
Result: 1.5 (Type: Double, Time taken: 1ms)
5 + 3 / 2
Result: 6.5 (Type: Double, Time taken: 4ms)
```
Perfect! What happens though if we want to resolve 5 + 3 before dividing by 2? Well, this would be achieved using
parentheses and will be the perfect introduction to statements. It will also be our first introduction to defining a
token using grammar as opposed to regular expressions.

Up until now, the tokens we've created have all represented types. Null, Decimal, Integer etc. Now though, we're
going to define one whose sole purpose is to perform a task based on one or more groups passed to it by the lexer.
These groups are defined in the token's grammar. Don't worry if this all sounds pretty confusing, so it might be best
to show you with our first statement token.

#### Expression Statement
Let's start by creating a new package within tokens called statements. We'll call it ExpressionToken.java as what
can be found within the brackets could be referred to as one:
```
...
[src]
   [main]
       [java]
           [com.aardvark]
               [config]
                   AardvarkConfig.java
               [operations]
                   IntegerOperation.java
               [tokens]
                   [operators]
                       AardvarkArithmeticOperator.java
                   [literals]
                       DecimalToken.java
                       IntegerToken.java
                       NullToken.java
                   [statements]
                       ExpressionToken.java
               Application.java
...
```
Open up the class and extend Token using the Void generic type. There are several differences between this and previous
tokens we've defined:
```java
public class ExpressionToken extends Token<Void> {

    public ExpressionToken() {
        super("Expression", null);
    }

    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new ExpressionToken());
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    @Override
    public String getPattern() {
        return "'(' expr ')'";
    }

    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase(")"))
            return Optional.of("An operation requires the contained logic to be wrapped in a pair of braces " + 
                    "e.g. ( <logic> )<--");
        return Optional.empty();
  }

    @Override
    protected List<Token<?>> process(LARDParser parser, LARDContext context, LARDConfig config) {
        return Collections.singletonList(
            parser.processExpression(getTokenGroups().get(0).getTokens(), context)
        );
    }
}
```
Let's break this down:
1. As we're using Void we simply pass null in the parent call from the constructor.
2. The definition in the ``createToken`` method has changed. Now we wrap the new instance within a superclass method
   called ``cloneDefaultProperties``. This allows the new instance to copy the pattern tokens generated from the grammar
   string to the new token. These are used by the lexer to keep track of position and assign lexed tokens into the
   tokens groups.
3. The pattern type has changed to GRAMMAR along with the actual pattern defined as ``'(' expr ')'``. I won't go into
   too much detail at this stage, but any value defined within single quotes is part of the syntax. The ``expr`` informs
   the lexer that it should expect one or more tokens in the capture group. I will cover grammar further once we expand
   more into other statements.
4. This is the first time we are actually using the ``getGuidance`` method. In this case there is only one situation
   that can occur. This is if the closing brace is not used and the statement is left open. As such, we'll provide some
   text to the developer about where they went wrong. I'll show an example in the runner below.
5. Finally, we come to the ``process`` method which is where the code is actually resolved. In this case it is very
   simple as we only have one token group to resolve a value from. Each token group can be fetched using the inherited
   ``getTokenGroups()`` method. This might be a bit hard to get your head around at this stage, so I would suggest trying
   out the ```(5 + 3) / 2``` example, once the token is configured, in the runner. If you debug the first line you'll
   notice that the token group has three tokens contained within:
   ```
   IntegerToken(5)
   AardvarkArithmeticOperator(+)
   IntegerToken(3) 
   ```
   To resolve that calculation, we simply pass that group back to the parser in the call ``parser.processExpression``.
   The responsibility of the current token only goes so far as to resolve its own logic. Everything else is deferred
   to the contained tokens or, in this case, the NumericOperation class we defined earlier. From there we simply return
   it in a collection.

Let's configure our new token in the configuration class:
```java
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected void initTokenHandlers() {
        //Literals
        addTokenHandler(new NullToken());
        addTokenHandler(new IntegerToken());
        addTokenHandler(new DecimalToken());

        //Statements
        addTokenHandler(new ExpressionToken());
    }
    //...
}
```
Now, let's run some examples:
```
(5 + 3) / 2
Result: 4 (Type: Integer, Time taken: 20ms)
5 + 3 / 2
Result: 6.5 (Type: Double, Time taken: 4ms)
3 / (1 + (3 * 3)) - 2
Result: -1.7 (Type: Double, Time taken: 4ms)
```
There are no restrictions on the level of depth statements can be nested.

Ok, let's review what we've achieved so far:
1. Created our project and runner
2. Literals (Null, Integer and Decimal)
3. Arithmetic Operators
4. Numeric Operation Handler
5. Written our first statement

From all of that effort we've got... a glorified calculator. Don't worry! With all the foundations we're laying,
development starts to accelerate exponentially. Soon you'll be able to add even complex statement with little effort.
It's now time to move on to our next statement which is a mainstay of most modern languages.

#### Ternary Conditional Token
I'm sure we're all familiar with these. They generally take the form ``condition ? trueResult : falseResult``.
We've got several issues to overcome though. Namely, how can we define a condition when we have no way to compare
values? More immediate though, how do we make LARD understand the condition result (Boolean)? Let's first start then
by defining a BooleanToken. If you're feeling confident writing your own token using what you learned, go ahead and
try it yourself. Alternatively, click the spoiler below for help:

<details> 
    <summary>BooleanToken Implementation</summary> 

First we define a new token called Boolean token in our ``tokens.literals`` package:
```
...
[src]
   [main]
       [java]
           [com.aardvark]
               [config]
                   AardvarkConfig.java
               [operations]
                   IntegerOperation.java
               [tokens]
                   [operators]
                       AardvarkArithmeticOperator.java
                   [literals]
                       BooleanToken.java
                       DecimalToken.java
                       IntegerToken.java
                       NullToken.java
                   [statements]
                       ExpressionToken.java
               Application.java
...
```
Next, let's define our BooleanToken by extending Token and passing the Boolean generic type:
```java
public class BooleanToken extends Token<Boolean> {

    public BooleanToken() { super("Boolean", false); }

    public BooleanToken(Boolean value) {
        super("Boolean", value);
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    @Override
    public String getPattern() {
        return "^(true|false)";
    }

    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    @Override
    public List<Token<?>> process(LARDParser parser, LARDContext context, LARDConfig config) {
        return Collections.singletonList(this);
    }

    @Override
    public Token<Boolean> createToken(String value) {
        return new BooleanToken(Boolean.parseBoolean(value));
    }
}
```
Finally, let's add it to the configuration:

```java
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected void initTokenHandlers() {
        //Literals
        addTokenHandler(new NullToken());
        addTokenHandler(new IntegerToken());
        addTokenHandler(new DecimalToken());
        addTokenHandler(new BooleanToken());        

        //Statements
        addTokenHandler(new ExpressionToken());
    }
    //...
}
```
Test in the runner:
```
true
Result: true (Type: Boolean, Time taken: 18ms)
false
Result: false (Type: Boolean, Time taken: 1ms)
```
</details>

Now that we've defined and configured booleans, we need to think about a different set of Operators. If you remember
back to the available operator sets, one of those was called the ComparisonOperator. Again, if you feel you know
how to do this using the AardvarkArithmeticOperator as a base, please do that. Alternatively, you can expand the
spoiler again for help:

<details> 
    <summary>AardvarkComparisonOperator Implementation</summary> 

Add a new class called AardvarkComparisonOperator.java to the ``tokens.operators`` package.
```
...
[src]
   [main]
       [java]
           [com.aardvark]
               [config]
                   AardvarkConfig.java
               [operations]
                   IntegerOperation.java
               [tokens]
                   [operators]
                       AardvarkArithmeticOperator.java
                       AardvarkComparisonOperator.java
                   [literals]
                       BooleanToken.java
                       DecimalToken.java
                       IntegerToken.java
                       NullToken.java
                   [statements]
                       ExpressionToken.java
               Application.java
...
```
Open up the new class and extend the ComparisonOperator template class. We'll stick with using the C / Java operators
for this:
```java
public class AardvarkComparisonOperator extends ComparisonOperator {

    public AardvarkComparisonOperator() {
        super(null);
    }

    @Override
    public Token<String> createToken(String value) {
        return new AardvarkComparisonOperator();
    }

    @Override
    public String getPattern() {
        return "^(==|!=|>=|<=|>|<)";
    }

    @Override
    protected void initOperators() {
        addOperator("==", ComparisonOperatorType.EQUAL);
        addOperator("!=", ComparisonOperatorType.NOT_EQUAL);
        addOperator(">=", ComparisonOperatorType.GREATER_EQUAL_TO);
        addOperator("<=", ComparisonOperatorType.LESS_EQUAL_TO);
        addOperator(">", ComparisonOperatorType.GREATER_THAN);
        addOperator("<", ComparisonOperatorType.LESS_THAN);
    }

    @Override
    public String[][] getOperatorOrder() {
        return new String[][] {
                {">=", "<=", ">", "<"},
                {"==", "!="}
        };
    }
}
```
Add the operator class to the configuration:
```java
@Override
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected void initOperators() {
        addOperatorHandler(new AardvarkArithmeticOperator(null));
        addOperatorHandler(new AardvarkComparisonOperator(null));
    }
    //...
}
```
</details>

We're one step closer to defining our statement. We've got the comparison operators defined and the result type. Let's
see what happens in the runner app:
```
1 == 1
dev.lard.exception.ParserException: Unable to handle numeric operation with operator '=='
	at dev.lard.languages.aardvark.operations.NumericOperation.handleCustomOperator(NumericOperation.java:34)
	at dev.lard.languages.aardvark.operations.NumericOperation.process(NumericOperation.java:53)
	at dev.lard.parser.LARDParser.processOp(LARDParser.java:321)
	at dev.lard.parser.LARDParser.processExpression(LARDParser.java:113)
	at dev.lard.parser.LARDParser.process(LARDParser.java:44)
	at dev.lard.processor.LARDProcessor.process(LARDProcessor.java:83)
	at dev.lard.runner.LARDRunner.run(LARDRunner.java:39)
	at dev.lard.runner.LARDRunner.run(LARDRunner.java:11)
	at dev.lard.languages.aardvark.AardvarkRunner.main(AardvarkRunner.java:11)
```
Yes, we need to update our NumericOperation class to handle those operators. Let's do that now:
```java
public class NumericOperation implements TypeOperation {
    //...
    @Override
    public Token<?> process(LARDConfig config, LARDContext context, Token<?> firstToken, OperatorHandler<?> operator,
                            Token<?> secondToken, Token<?> leftSide) {
        BigDecimal first = new BigDecimal(firstToken.getValue().toString());
        BigDecimal second = new BigDecimal(secondToken.getValue().toString());
        if (operator instanceof ArithmeticOperator) {
            switch (((ArithmeticOperator)operator).getOpType()) {
                case DIVIDE: return convert(first.divide(second, 6, RoundingMode.HALF_UP));
                case ADD: return convert(first.add(second));
                case SUBTRACT: return convert(first.subtract(second));
                case MULTIPLY: return convert(first.multiply(second));
                case MODULO: return convert(first.remainder(second));
                default: return handleCustomOperator(firstToken, operator, secondToken);
            }
        } else if (operator instanceof ComparisonOperator) {
            switch (((ComparisonOperator)operator).getOpType()) {
                case GREATER_EQUAL_TO: return new BooleanToken(first.compareTo(second) >= 0);
                case LESS_EQUAL_TO: return new BooleanToken(first.compareTo(second) <= 0);
                case NOT_EQUAL: return new BooleanToken(first.compareTo(second) != 0);
                case EQUAL: return new BooleanToken(first.compareTo(second) == 0);
                case GREATER_THAN: return new BooleanToken(first.compareTo(second) > 0);
                case LESS_THAN: return new BooleanToken(first.compareTo(second) < 0);
            }
        }
        return handleCustomOperator(firstToken, operator, secondToken);
    }
    //...
}
```
In the case of the comparison operators, we can simply return the result as a BooleanToken with no need to use the
wrap method. Let's run some tests:
```
4 + 4 <= 6 + 2
Result: true (Type: Boolean, Time taken: 19ms)
10 == 5 + 5
Result: true (Type: Boolean, Time taken: 1ms)
3 < 4
Result: true (Type: Boolean, Time taken: 1ms)
9 > 10
Result: false (Type: Boolean, Time taken: 1ms)
13 != 13
Result: false (Type: Boolean, Time taken: 1ms)
```
Great, so we've got the condition portion of our statement covered. Let's write our statement class. Add a new token
called TernaryConditionalToken.java (or in my case ConditionalToken.java) to the ``tokens.statements`` package. We'll
then define the class as such:
```java
public class ConditionalToken extends Token<Void> {

    public ConditionalToken() {
        super("Conditional", null);
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.GRAMMAR;
    }

    @Override
    public String getPattern() {
        return "expr '?' expr ':' expr";
    }

    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        if (token.equalsIgnoreCase(":"))
            return Optional.of("A conditional requires true / false outcomes to be separated by a ':' " +
                    "e.g. a > b ? a :<-- b");
        return Optional.empty();
    }

    @Override
    public List<Token<?>> process(LARDParser parser, LARDContext context, LARDConfig config) {
        //Expect that there are 3 tokens groups representing the condition and true / false token groups
        if (getTokenGroups().size() < 3) {
            String found = getTokenGroups().stream().map(Token::toSimpleString).collect(Collectors.joining(","));
            throw new ParserException(String.format("Condition does not have required arguments to execute. Expecting " +
                    "3 groups being condition, trueResult and falseResult. Found: [%s]", found));
        }
        //Evaluate the condition using the tokens found in the first token group
        Token<?> conditionResult = parser.processExpression(getTokenGroups().get(0).getTokens(), context);
        //If the condition is not a Boolean then throw an error i.e. "1 + 2 ? 3 : 4"
        if (!(conditionResult instanceof BooleanToken)) {
            throw new ParserException(String.format("Expected a boolean result from condition '%s'. Possible invalid " +
                            "condition specified", getTokenGroups().get(0)));
        }
        //Execute the relevant set of tokens based on the condition result
        return Collections.singletonList((((BooleanToken) conditionResult).getValue()) ?
                parser.processExpression(getTokenGroups().get(1).getFlatTokens(), context) :
                parser.processExpression(getTokenGroups().get(2).getFlatTokens(), context));
    }

    @Override
    public Token<Void> createToken(String value) {
        return cloneDefaultProperties(new ConditionalToken());
    }

    @Override
    public String toString() {
        return "Conditional{" +
                "condition=" + (!getTokenGroups().isEmpty() ? getTokenGroups().get(0) : null) +
                ", trueResult=" + (getTokenGroups().size() > 1 ? getTokenGroups().get(1) : null) +
                ", falseResult=" + (getTokenGroups().size() > 2 ? getTokenGroups().get(2) : null) +
                '}';
    }
}
```
Ok, that's a big chunk of code but let's break it down:
1. The pattern now defines three capture groups of one or more tokens. This means we can do something like
   ``1 + 2 > 3 ? 4 + 4 : 3 / 2``. After the first capture group we're expecting a '?' character with a ':' expected
   between true / false cases.
2. We have one guidance condition if a user defines the first and second capture including the '?' value, but no
   subsequent ':' is defined, this will trigger this scenario.
3. The process is actually quite straightforward. Since the pattern has defined three capture groups, if there are
   less than 3 returned by the lexer then we throw an exception. The next line:
    ```java
    Token<?> conditionResult = parser.processExpression(getTokenGroups().get(0).getTokens(), context);
    ```
   Passes evaluation of the condition (the first token group) back to∫******** the parser for a result. We then check to see
   if the result is a BooleanToken, otherwise we'll throw an error. For example, defining ``1 + 1 ? 1 : 2`` would
   cause this error to be thrown. I've aggregated a few steps into the final line, but effectively we evaluate the
   result of the condition using Java's own ternary condition operator. In each scenario, we pass the relevant
   token group to the parser for evaluation and return the result in a list.
4. I've overridden the ``toString()`` here as it's useful to do this for debugging purposes. It makes the content
   of the token easier tor read.

Ok, let's add our new Token as a new token handler to the configuration:
```java
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected void initTokenHandlers() {
        //Literals
        addTokenHandler(new NullToken());
        addTokenHandler(new IntegerToken());
        addTokenHandler(new DecimalToken());
        addTokenHandler(new BooleanToken());        

        //Statements
        addTokenHandler(new ExpressionToken());
        addTokenHandler(new ConditionalToken());
    }
    //...
}
```
Let's try evaluating some conditionals:
```
3 > 4 ? 1 : 2
Result: 2 (Type: Integer, Time taken: 22ms)
4 > 3 ? 1 : 2
Result: 1 (Type: Integer, Time taken: 3ms)
1 + (3 * 2) > 9 - 2 ? 1 : 2
Result: 2 (Type: Integer, Time taken: 4ms)
```
#### String Token
Up until now we've been dealing solely with numbers and indirectly booleans, but let's see how easy it is to add
String support to use with our statements. **NOTE**: The following implementation goes over and above what a String
needs to achieve, however I feel it's important to bring up these features should you use them elsewhere in your tokens.
With that being said, let's get started.

In our case we'll add String support for both single and double-quote characters. This will require a slightly different
approach in our token which I'll explain below. Firstly, let's add a new StringToken.java class to our ``tokens.literals``
package. We'll then define the following:
```java
public class StringToken extends Token<String> {

    public StringToken() { super("String", null); }

    public StringToken(String value) {
        super("String", value);
    }

    @Override
    public PatternType getPatternType() {
        return PatternType.REGEX;
    }

    @Override
    public String getPattern() {
        return "^(\"(.*?[^\\\\])\"|'(.*?[^\\\\])')";
    }

    @Override
    public Optional<String> getGuidance(String token, List<Integer> groupsCount) {
        return Optional.empty();
    }

    @Override
    public Optional<Token<String>> match(Expression expression, boolean dryRun) {
        Matcher m = getMatcher(expression.getValue());
        if (m.find()) {
            if (!dryRun) expression.cutTo(m.end(0));
            String value = Objects.isNull(m.group(2)) ? m.group(3) : m.group(2);
            value = value.replace("\\\"", "\"");
            value = value.replace("\\'", "'");
            return Optional.of(createToken(value));
        }
        return Optional.empty();
    }

    @Override
    public List<Token<?>> process(LARDParser parser, LARDContext context, LARDConfig config) {
        return Collections.singletonList(this);
    }

    @Override
    public Token<String> createToken(String value) {
        return new StringToken(value);
    }

    @Override
    public String toString() {
        return String.format("'%s'", getValue());
    }
}
```
There are a couple of differences here which I'll explain:

<span style="text-decoration:underline">Regular Expression</span>

Considering a String is just some text surrounding with a character (``'`` or ``"``), why is the definition so
complicated? Ignoring the Java escape backslashes, the actual expression is ``^(\"(.*?[^\\])\"|'(.*?[^\\])')``. Looking
at it, we can see that it starts with the typical ``^`` which means the beginning of a String. We then have a set of
brackets which represent a capture group.

Half way through the capture group you'll see a ``|`` which acts as a logical or, meaning it can match the pattern
found before or after. Since both sides are the same besides the character used, we'll look at the first of these
``"(.*?[^\\])"``. We start with a ``"`` in this case and have another set of brackets for a group to capture the text
content found within. ``.*`` means match any character (``.``) 0 or more times (``*``). The next ``?`` means match as
few characters as possible. We then have a group of characters which, as they start with a ``^`` character mean they
are to be ignored. Why would we do this? Well, say we want to embed a String within a String, the only way for this
to happen would be to use an escape character. In this case we're using the backslash to represent that.

As such, we should be able to support both single and double-quoted Strings, but also those which contain escaped
String quotations within the Strings themselves! Because we're using that logical or though, we do need to modify
the way in which we map the capture groups. As such, this brings us onto the next change.

<span style="text-decoration:underline">Match Method</span>

This method gets called when the lexer is trying to find a match for the next token. Typically it would use the single
capture group defined (index 1). In our case though, we've defined not just one, but embedded groups two and three
representing both sides of the or for ``"`` and ``'`` respectively. As such, we need to find which group returns a
match and use that as our value.

The majority of this code matches the method from which it overrides. The first line creates a regular expression
matcher object from the pattern and returns it. We then determine if a match has been found and if so checks a
dry-run boolean flag. This flag is used when we want to check for a match, but not affect the expression itself. If
it is not a dry-run it removes the matched portion of text from the beginning of the expression. We then check capture
group 2 (``"``) for a match and if none is found then we'll defer to group 3 (``'``). I've extracted the block of
code we're using to select the group, modify it and pass it to become a new instance of the StringToken to be returned
to the lexer:

```
String value = Objects.isNull(m.group(2)) ? m.group(3) : m.group(2);
value = value.replace("\\\"", "\"");
value = value.replace("\\'", "'");
return Optional.of(createToken(value));
```

Unfortunately I am not aware of a method to remove characters for this scenario via regex. As such, we use a standard
``String.replace`` to remove the backslashes from our matched String. If you didn't manage to follow what I wrote above,
it's not strictly necessary to understand all of this, but if you're interested I would suggest adding a breakpoint to
the method to get a better idea of how it works.

Ok, time to test it by adding it to our configuration and giving it a try in the expression runner:
```java
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected void initTokenHandlers() {
        //Literals
        addTokenHandler(new NullToken());
        addTokenHandler(new IntegerToken());
        addTokenHandler(new DecimalToken());
        addTokenHandler(new BooleanToken());  
        addTokenHandler(new StringToken());

        //Statements
        addTokenHandler(new ExpressionToken());
        addTokenHandler(new ConditionalToken());
    }
    //...
}
```
Runner:
```
'hello'
Result: hello (Type: String, Time taken: 11ms)
"hello world!"
Result: hello world! (Type: String, Time taken: 1ms)
"The alien said \"zee oot!\", then flew off"
Result: The alien said "zee oot!", then flew off (Type: String, Time taken: 1ms)
```
So far so good, but let's take things further by adding String operator support. To do this we'll start by adding a
new StringOperation class to our operations package. Given the recent additions, the package structure and classes
should look similar to the following:
```
...
[src]
   [main]
       [java]
           [com.aardvark]
               [config]
                   AardvarkConfig.java
               [operations]
                   NumericOperation.java
                   StringOperation.java
               [tokens]
                   [operators]
                       AardvarkArithmeticOperator.java
                       AardvarkComparisonOperator.java
                   [literals]
                       BooleanToken.java
                       DecimalToken.java
                       IntegerToken.java
                       NullToken.java
                       StringToken.java
                   [statements]
                       ExpressionToken.java
               Application.java
...
```
In our operation class we define that either the first or second argument can be a String. This differs to other
operation classes as you'd typically want them to be in that format i.e. Booleans or Numbers. In our case though
we can use the passed Tokens value and call the ``toString()`` on it. As such, we can append whatever type it is
either before or after our defined String. 

We'll add support for 3 operators for use with our String type. These will be add, equal and not-equal. You are
however free to add your own additional operators. For example, you could make use of subtract by removing all 
instances of the right String from the original e.g. ``'William' - 'iam'`` would result in ``'Will'``. This could
be done by using 
```java
    case SUBTRACT: return new StringToken(first.getValue().toString()
                        .replace(second.getValue().toString(), ""));
```
For this though, we'll keep things simple. It's worth noting here again that we're using multiple operator types.
If you do want to use a specific one, please ensure you are casting the ``operator`` to the correct type.
```java
public class StringOperation implements TypeOperation {

    @Override
    public boolean canHandle(Token<?> first, OperatorHandler<?> operator, Token<?> second) {
        //Other types (left or right side) are cast to Strings in operations
        return first.is(String.class) || second.is(String.class);
    }

    @Override
    public <T> T handleCustomOperator(Token<?> first, OperatorHandler<?> operator, Token<?> second) {
        throw new ParserException(String.format("Unable to handle String operation with operator '%s'",
                operator.getValue()));
    }

    @Override
    public Token<?> process(LARDConfig config, LARDContext context, Token<?> first, OperatorHandler<?> operator,
                            Token<?> second, Token<?> leftSide) {
        if (operator instanceof ArithmeticOperator) {
            switch (((ArithmeticOperator)operator).getOpType()) {
                case ADD: return new StringToken(first.getValue().toString()
                        .concat(second.getValue().toString()));
            }
        } else if (operator instanceof ComparisonOperator) {
            switch (((ComparisonOperator)operator).getOpType()) {
                case EQUAL: return new BooleanToken(first.getValue(String.class)
                        .equalsIgnoreCase(second.getValue(String.class)));
                case NOT_EQUAL: return new BooleanToken(!first.getValue(String.class)
                        .equalsIgnoreCase(second.getValue(String.class)));
            }
        }
        return handleCustomOperator(first, operator, second);
    }
}

```
Let's add our new operation class to the config:
```java
public class AardvarkConfig extends LARDConfig {
    //...
    @Override
    protected TypeOperation initTypeOperations() {
        addTypeOperation(new NumericOperation());
        addTypeOperation(new StringOperation());
        return null;
    }
    //...
}
```
Finally, let's run some examples:
```
'hello ' + 'john'
Result: hello john (Type: String, Time taken: 15ms)
'abc' == 'abc'
Result: true (Type: Boolean, Time taken: 1ms)
'abc' == 'bdc'
Result: false (Type: Boolean, Time taken: 1ms)
'Johns age: ' + 41
Result: Johns age: 41 (Type: String, Time taken: 1ms)
```
**NOTE**: The last example is mixing String's with Integer's, but that is no issue as previously mentioned as we're
simply using the ``toString()`` on the Tokens value. This allows us some flexibility without having to specifically
handle these scenarios.

#### Loops
One of the mainstay features of any language is a loop. They come in a number of varieties, but to get you started
we'll concentrate on the for and for-each loops.
